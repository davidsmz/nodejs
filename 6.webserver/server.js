const express = require('express');
const app = express();
const hbs = require('hbs');

require('./hbs/helpers');

app.use(express.static(__dirname + '/public'));

// Express HBS engine
hbs.registerPartials(__dirname + '/views/partials'); // partials
app.set('view engine', 'hbs');

// helpers

// hbs.registerHelper('getYear', () => {
//   return new Date().getFullYear();
// });

// hbs.registerHelper('capitalizar', (name) => {

//   let palabras = name.split(' ');
//   palabras.forEach((palabra, index) => {
//     palabras[index] = palabra.charAt(0).toUpperCase() + palabra.slice(1).toLowerCase();
//   });
//   return palabras.join(' ');
// });


// app.get('/', (req, res) => {
//   // res.send('Hello World');
//   let salida = {
//     nombre: 'David',
//     apellido: 'Smz',
//     edad: 25,
//     url: req.url
//   }
//   res.send(salida);
// });

app.get('/', (req, res) => {
  res.render('home', {
    name: 'david'
  });
});

app.get('/about', (req, res) => {
  res.render('about');
});

app.listen(8080, () => {
  console.log('Escuchando peticiones en el puerto 8080');
});