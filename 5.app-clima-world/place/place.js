const axios = require('axios');

const getLugarLatLng = async (direccion) => {
  let encodeUrl = encodeURI(direccion);

  let resp = await axios.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${encodeUrl}&key=AIzaSyDzbQ_553v-n8QNs2aafN9QaZbByTyM7gQ`);

  if (resp.data.status === 'ZERO_RESULTS') {
    throw new Error(`No hay resultados para la ciudad ${direccion}`)
  }
  // console.log(JSON.stringify(resp.data, undefined, 2)); // formato json

  let results = resp.data.results[0];
  // let address = results.formatted_address;
  // let location = results.geometry.location;
  // let lat = location.lat;
  // let lng = location.lng;

  let {formatted_address: address, geometry: {location: {lat, lng}}} = results;
  
  // console.log('Direccion: ', direccion);
  // console.log('lat: ', latitude);
  // console.log('lng: ', longitud);

  return {
    address,
    lat,
    lng
  }

}


module.exports = {
  getLugarLatLng
}

