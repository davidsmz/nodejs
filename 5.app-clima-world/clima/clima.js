const axios = require('axios');


let getClima = async (lat, lng) => {
  let resp = await axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lng}&units=metric&appid=339ff654cf4aafc4a119f15f15b1f195`);

  let dataClima = resp.data;
  let {main: {temp: temperatura}} = dataClima;

  return temperatura;

}

module.exports = {
  getClima
}