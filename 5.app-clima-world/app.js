const place = require('./place/place');

const argv = require('./config/yargs').argv;

const clima = require('./clima/clima');

// place.getLugarLatLng(argv.direccion)
//               .then(resp => {
//                 let {lat, lng} = resp;
//                 clima.getClima(lat, lng);
//               })
//               .catch(err => console.log('Error: ', err));


let getInfo = async (direccion) => {

  try {

    let location = await place.getLugarLatLng(direccion);

    let { address, lat, lng } = location;

    let temperatura = await clima.getClima(lat, lng);

    return `El clima en ${address} es ${temperatura} °C`;

  } catch (error) {
    return `No se pudo determinar el clima en ${direccion}`;
  }
  

}

getInfo(argv.direccion)
  .then(msg => console.log(msg))
  .catch(err => console.log(err));