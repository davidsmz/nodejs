function saludar(nombre) {
  let mensaje = `Hola ${nombre}`;

  return mensaje;
}

let saludo = saludar('David');

console.log(saludo);

// Resultado de consola
/*
David
*/


/**
 * Pasos Ocurridos en este proceso
 * 1. Se ejecuta la funcion main() en la pila de procesos
 * 2. Se registra la funcion saludar en la pila de procesos
 * 3. se ejecuta el codigo de la funcion saludar y se alamcena resultado en saludo
 * 4. se ejecuta la funcion console.log()
 * 5. Se termina el proceso main() y se elimina de la pila de procesos
 */