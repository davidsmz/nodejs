console.log('Inicio del programa');

setTimeout(() => {
  console.log('Primer Timeout')
}, 3000);

setTimeout(() => {
  console.log('Segundo Timeout')
}, 0);

setTimeout(() => {
  console.log('Tercer Timeout')
}, 0);

console.log('Fin del programa');

// Resultado de consola
/* 
Inicio del programa
Fin del programa
Segundo Timeout
Tercer Timeout
Primer Timeout
*/

/**
 * Pasos Ocurridos en este proceso
 * 01. Se ejecuta la funcion main() en la pila de procesos
 * 
 * 02. Se ejecuta el console.log('Inicio del programa');
 * 
 * 03. Se ejecuta el primer setTimeout y pasa a la node apis
 * 
 * 04. Se ejecuta el segundo setTimeout y pasa la node apis
 * 
 * 05. Se ejecuta el tercer setTimeout y pasa la node apis
 * 
 * 06. El segundo setTimeout pasa a la pila de callbacks y espera para su ejecucion
 * 
 * 07. Se ejecuta el console.log('Fin del programa');
 * 
 * 08. Ya no hay nada más en el main() por eso se termina y elimina de la pila de procesos
 * 
 * 09. El tercer setTimeout pasa a la pila de callbacks y espera para su ejecucion
 * 
 * 10. Node revisa la pila de procesos y como no hay nada revisa la pila de callbacks
 * 
 * 11. Se ejecuta el Segundo setTimeout en la pila de procesos por ser el primero en estar 
 *     dispuesto a ejecutarse en la pila de callbacks y por ende se ejecuta 
 *     console.log('Segundo Timeout') y elimina
 * 
 * 12. Se ejecuta el Tercer setTimeout en la pila de procesos por ser el segundo en estar 
 *     dispuesto a ejecutarse en la pila de callbacks y por ende se ejecuta 
 *     console.log('Tercer Timeout') y elimina
 * 
 * 13. Una vez pasa 3 segundos el tercer setTimeout pasa a la pila de callbacks y espera para su 
 *     ejecucion
 * 
 * 14. Se ejecuta el Primer setTimeout en la pila de procesos por ya estar dispuesto a ejecutarse
 *     en la pila de callbacks y por ende se ejecuta 
 *     console.log('Tercer Timeout') y elimina
 * 
 * 15. Node revisa que esta vacio todas las pilas y alli termina el proceso.
 */