// requires
var colors = require('colors');
const argv = require('./config/yargs').argv;
// const fs = require('fs');
// const express = require('express'); // require de paquete de npm
// const fs = require('./path/'); // require de archivo de nuestro proyecto

const {crearArchivo} = require('./multiplicar/multiplicar');
const {listarTabla} = require('./multiplicar/listar');

let base = 3;
// let data = '';
// for (let i = 1; i <= 10; i++) {
//   data += `${base} * ${i} = ${base * i}\n`;
// }





// fs.writeFile(`tablas/tabla-${base}.txt`, data, (err) => {
//   if (err) throw err;
//   console.log(`El archivo tabla-${base} ha sido creado`);
// });

// let argv = process.argv;
// let parametro = argv[2];
// base = parametro.split('=')[1];
// console.log(base)

console.log(argv)
// console.log(argv.base);

// crearArchivo(base)
//   .then((archivo) => console.log(`El archivo ${archivo} ha sido creado`))
//   .catch((error) => console.log('Error: ', error));

let comando = argv._[0];

switch (comando) {
  case 'listar':
    // console.log('listar');
    listarTabla(argv.base, argv.limite);
    break;
  case 'crear':
    // console.log('crear');
    crearArchivo(argv.base, argv.limite)
      .then((archivo) => console.log(`El archivo ${archivo} ha sido creado`.green))
      .catch((error) => console.log('Error: ', error));
    break;
  default:
    console.log('Comando no reconocido');
    break;
}
