
// Primer tipo de callbacks en un setTimeout
setTimeout(() => {
  console.log('Hola mundo');
}, 3000);

// Ejemplo de callback
let getUsuarioById = (id, callback) => {

  let usuario = {
    nombre: 'David',
    id
  }

  if (id === 20) {
    console.log(`El usuario con id ${id} no existe en la BD`);
  }else {
    callback(null, usuario);
  }

}

getUsuarioById(21, (err, usuario) => {

  if (err) {
    return console.log(err);
  }

  console.log('usuario de base de datos ', usuario);
})