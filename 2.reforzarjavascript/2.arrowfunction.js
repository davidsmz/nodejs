
let a = 10;
let b = 20;

// funcion normales
function sumar1(a, b) {
  return a + b;
}

//funcion sumar en una funcion flecha
let sumar2 = (a, b) => {
  return a + b;
}

// si el retorno de la funcion es una sola linea se puede transformar
let sumar3 = (a, b) => a + b;

// si solo se tiene un argumento se puede simplificar asi
let sumar4 = c => a + b + c;
console.log(sumar1(a, b));
console.log(sumar2(a, b));
console.log(sumar3(a, b));
console.log(sumar4(0));


// En un objeto se utiliza funciones normales y no flecha
let deadpool = {
  name: 'Wade',
  lastname: 'Winston',
  power: 'regeneracion',
  getNombre: function () {
    return `${this.name} ${this.lastname} - power: ${this.power}`;
  }
}


