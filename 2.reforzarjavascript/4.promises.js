let empleados = [{
  id: 1,
  nombre: 'David'
}, {
  id: 2,
  nombre: 'Raquel'
}, {
  id: 3,
  nombre: 'Caleb'
}];


let salarios = [{
  id: 1,
  salario: 1000
}, {
  id: 2,
  salario: 2000
}];

let getSalario = (empleado) => {

  return new Promise((resolve, reject) => {
    let salarioDB = salarios.find(salario => salario.id === empleado.id);

    if (!salarioDB) {
      reject(`No se encontro un salario para el usuario ${empleado.nombre}`);
    } else {
      resolve({
        nombre: empleado.nombre,
        salario: salarioDB.salario
      });
    }
  })

}


let getEmpleado = (id) => {
  
  return new Promise((resolve, reject) => {
    let empleadoDB = empleados.find(empleado => {
      return empleado.id === id;
    });

    // console.log(empleadoDB);

    if (!empleadoDB) {
      reject(`No existe un empleado con el id ${id}`)
    } else {
      resolve(empleadoDB);
    }
  })
  
}


getEmpleado(3).then((empleado) => {

  // console.log('Empleado de BD ', empleado);

  getSalario(empleado).then((resp) => {
    console.log(`el usuario ${resp.nombre} tiene un salario de $${resp.salario}`)
  }).catch((err) => {
    console.log(err)
  });

}).catch((err) => {
  console.log(err)
});