let empleados = [{
  id: 1,
  nombre: 'David'
},{
  id: 2,
  nombre: 'Raquel'
},{
  id: 3,
  nombre: 'Caleb'
}];

let salarios = [{
  id: 1,
  salario: 1000
},{
  id: 2,
  salario: 2000
}];



// Si encuentra el salario para una persona imprime:
// {nombre: 'David', salario: '1000'}
// Si no se encuentra el salario:
// No se encontro un salario para el usuario David

let getSalario = (empleado, callback) => {
  let salarioDB = salarios.find(salario => salario.id === empleado.id);

  if (!salarioDB) {
    callback(`No se encontro un salario para el usuario ${empleado.nombre}`);
  } else {
    callback(null, {
      nombre: empleado.nombre,
      salario: salarioDB.salario
    });
  }
}


let getEmpleado = (id, callback) => {

  let empleadoDB = empleados.find(empleado => {
    return empleado.id === id;
  });

  // console.log(empleadoDB);

  if (!empleadoDB) {
    callback(`No existe un empleado con el id ${id}`)
  }else {
    callback(null, empleadoDB);
  }
}



getEmpleado(3, (err, empleado) => {

  if (err) {
    return console.log(err)
  }

  // console.log(empleado);

  getSalario(empleado, (err, resp) => {
    if (err) {
      return console.log(err);
    }

    console.log(`el usuario ${resp.nombre} tiene un salario de $${resp.salario}`)
  })

});




// getSalario(empleados[2], (err, empleadoSalario) => {
//   if (err) {
//     return console.log(err);
//   }

//   console.log(empleadoSalario);
// });

