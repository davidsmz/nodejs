let deadpool = {
  name: 'Wade',
  lastname: 'Winston',
  power: 'regeneracion',
  getNombre: function () {
    return `${this.name} ${this.lastname} - power: ${this.power}`;
  }
}

// let name = deadpool.name;
// let lastname = deadpool.lastname;
// let power = deadpool.power;

// En este caso se crean las variables que contienen las propiedades del objeto
let {name, lastname, power} = deadpool;
console.log(name, lastname, power);

// Vamos a cambiar el nombre de la variables a otras distintas del nombre de las propiedades
let {name: nombre, lastname: apellido, power: poder} = deadpool;
console.log(nombre, apellido, poder)
