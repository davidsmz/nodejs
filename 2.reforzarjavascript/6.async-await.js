
// Funcion con una promesa

// let getNombre = () => {
//   return new Promise((resolve, reject) => {
//     resolve('David');
//   })

// }


// Convertimos esa funcion a async - await

let getNombre = async() => {

  // cualquier error en javascript dentro de la funcion es error en asyn await
  // para poder indicar errores

  return 'David';
}

//console.log(getNombre()); // Retorna una promesa
getNombre()
  .then((nombre) => {
    console.log(nombre)
  })
  .catch((err) => {
    console.log('Error Async')
  });


let getApellido = () => {
  return new Promise((resolve, reject) => {
    
    setTimeout(() => {
      resolve('Smz');
    }, 500);

  })

}

getApellido()
  .then((lastname) => {
    console.log(lastname);
  })
  .catch((error) => {
    console.log('Error: ', error)
  });

let getSaludo = async () => {
  let nombre = await getNombre();
  let apellido = await getApellido();

  return `Hola ${nombre} ${apellido}`;
}

getSaludo().then((saludo) => {
    console.log(saludo);
});

// Resolver problema de empleados y Salario

let empleados = [{
  id: 1,
  nombre: 'David'
}, {
  id: 2,
  nombre: 'Raquel'
}, {
  id: 3,
  nombre: 'Caleb'
}];


let salarios = [{
  id: 1,
  salario: 1000
}, {
  id: 2,
  salario: 2000
}];


let getEmpleado = async (id) => {

  let empleadoDB = empleados.find(empleado => {
    return empleado.id === id;
  });

  if (!empleadoDB) {
    throw new Error(`No existe un empleado con el id ${id}`)
  } else {
    return empleadoDB
  }

}


let getSalario = (empleado) => {

  let salarioDB = salarios.find(salario => salario.id === empleado.id);

  if (!salarioDB) {
    throw new Error(`No se encontro un salario para el usuario ${empleado.nombre}`);
  } else {
    return {
      nombre: empleado.nombre,
      salario: salarioDB.salario
    };
  }

}


let getInformacion = async (id) => {
  let empleado = await getEmpleado(id);
  let objEmpleado = await getSalario(empleado);

  return `usuario ${objEmpleado.nombre} tiene un salario de $${objEmpleado.salario}`
}

getInformacion(4)
  .then(msg => console.log(msg))
  .catch(error => console.log('Error: ', error));