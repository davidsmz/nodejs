const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

let rolesvalidate = {
  values: ['ADMIN_ROLE', 'USER_ROLE'],
  message: '{VALUE} not role validate'
};

const Schema = mongoose.Schema;

let userSchema = new Schema({
  name: {
    type: String,
    required: [true, 'The name id necesary']
  },
  email: {
    type: String,
    unique: true,
    required: [true, 'The email id necesary']
  },
  password: {
    type: String,
    required: [true, 'The password id necesary']
  },
  img: {
    type: String,
    required: false
  },
  role: {
    type: String,
    default: 'USER_ROLE',
    enum: rolesvalidate
  },
  status: {
    type: Boolean,
    default: true
  },
  google: {
    type: Boolean,
    default: false
  }
});

// Siempre se llama cuando se intenta imprimir
userSchema.methods.toJSON = function() {
  let user = this;
  let userObject = user.toObject();
  delete userObject.password;

  return userObject;
}

userSchema.plugin(uniqueValidator, {message: '{PATH} should be unique'});

module.exports = mongoose.model('User', userSchema);

