// ======================
// ---------port---------
// ======================

process.env.PORT = process.env.PORT || 3000;

// ======================
// ------Enviroment------
// ======================

process.env.NODE_ENV = process.env.NODE_ENV || 'env';

// ======================
// -----Expire Token-----
// ======================

// 60 seg, 60 min, 24 h, 30 d
process.env.EXPIRE_TOKEN = 60 * 60 * 24 * 30;

// ======================
// ---------SEED---------
// ======================

process.env.SEED = process.env.SEED || 'secret_or_seed';

// ======================
// -----URL Database-----
// ======================

let urlDB;

if (process.env.NODE_ENV === 'env') {
  urlDB = 'mongodb://localhost:27017/cafe';
} else {
  urlDB = 'mongodb://davidsmz:ca155634leb93@ds213255.mlab.com:13255/udemy_cafe';
}

// let urlDB = 'mongodb://localhost:27017/cafe'

// let urlDB = 'mongodb://davidsmz:ca155634leb93@ds213255.mlab.com:13255/udemy_cafe'

process.env.URLDB = urlDB;

// ======================
// ---Google ClientId----
// ======================
process.env.CLIENT_ID =
  process.env.CLIENT_ID ||
  '69400654054-1bnj35g1e058fu1cd6c6b44skkfa80kc.apps.googleusercontent.com';
