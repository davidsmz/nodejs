const express = require('express')

const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const { OAuth2Client } = require('google-auth-library')
const client = new OAuth2Client(process.env.CLIENT_ID)
const User = require('../models/user')

const app = express()

app.post('/login', (req, res) => {
  let body = req.body

  // conditions in findOne
  let conditions = { email: body.email }

  User.findOne(conditions, (err, userDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      })
    }

    if (!userDB) {
      return res.status(400).json({
        ok: false,
        err: {
          message: '(user) or password incorrect'
        }
      })
    }

    // match password in bcrypt
    if (!bcrypt.compareSync(body.password, userDB.password)) {
      return res.status(400).json({
        ok: false,
        err: {
          message: 'user or (password) incorrect'
        }
      })
    }

    let payload = {
      user: userDB
    }

    let seed = process.env.SEED

    let expired = { expiresIn: process.env.EXPIRE_TOKEN }

    // let token = jwt.sign({
    //   user: userDB
    // }, 'there_is_seed', {expiresIn: 60 * 60 * 24 * 30});

    let token = jwt.sign(payload, seed, expired)

    res.json({
      ok: true,
      user: userDB,
      token
    })
  })
})

// Google config
async function verify(token) {
  const ticket = await client.verifyIdToken({
    idToken: token,
    audience: process.env.CLIENT_ID // Specify the CLIENT_ID of the app that accesses the backend
    // Or, if multiple clients access the backend:
    //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
  })
  const payload = ticket.getPayload()
  // const userid = payload['sub'];
  // If request specified a G Suite domain:
  //const domain = payload['hd'];
  // console.log(payload.name)
  // console.log(payload.email)
  // console.log(payload.picture)
  return {
    name: payload.name,
    email: payload.email,
    picture: payload.picture,
    google: true
  }
}
// verify().catch(console.error);

app.post('/google', async (req, res) => {
  const token = req.body.idtoken

  const googleUser = await verify(token).catch(err => {
    return res.status(403).json({
      ok: false,
      err
    })
  })

  try {
    const userDB = await User.findOne({ email: googleUser.email })
    if (userDB) {
      if (userDB.google == false) {
        return res.json({
          ok: false,
          err: {
            message: 'Sigin with email register'
          }
        })
      } else {
        const payload = {
          user: userDB
        }

        const seed = process.env.SEED

        const expired = { expiresIn: process.env.EXPIRE_TOKEN }

        // let token = jwt.sign({
        //   user: userDB
        // }, 'there_is_seed', {expiresIn: 60 * 60 * 24 * 30});

        const token = jwt.sign(payload, seed, expired)

        return res.json({
          ok: true,
          user: userDB,
          token
        })
      }
    } else {
      let user = new User()
      user.name = googleUser.name
      user.email = googleUser.email
      user.img = googleUser.img
      user.google = true
      user.password = 'nothing'

      const userDB = await user.save()
      const payload = {
        user: userDB
      }

      const seed = process.env.SEED

      const expired = { expiresIn: process.env.EXPIRE_TOKEN }

      // let token = jwt.sign({
      //   user: userDB
      // }, 'there_is_seed', {expiresIn: 60 * 60 * 24 * 30});

      const token = jwt.sign(payload, seed, expired)

      return res.json({
        ok: true,
        user: userDB,
        token
      })
    }
  } catch (err) {
    return res.json({
      ok: false,
      err
    })
  }
})

module.exports = app
