const express = require('express');
const bcrypt = require('bcrypt');
const _ = require('underscore');
const User = require('../models/user');
const { checkToken, checkAdmin_Role } = require('../middlewares/authorization');

const app = express();

// app.get('/', function (req, res) {
//   res.json('Hello World')
// });

app.get('/user', checkToken, (req, res) => {
  let from = req.query.from || 0;
  from = Number(from);

  let limit = req.query.limit || 5;
  limit = Number(limit);

  User.find({ status: true }, 'name email')
    .limit(limit)
    .skip(from)
    .exec((err, users) => {
      if (err) {
        return res.status(400).json({
          ok: false,
          err
        });
      }

      User.countDocuments({ status: true }, (err, count) => {
        res.json({
          ok: true,
          users,
          conteo: count
        });
      });
    });

  // res.json('get Usuario');
});

app.post('/user', [checkToken, checkAdmin_Role], (req, res) => {
  let body = req.body;

  let user = new User({
    name: body.name,
    email: body.email,
    password: bcrypt.hashSync(body.password, 10),
    role: body.role
  });

  user.save((err, userDB) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      user: userDB
    });
  });

  // res.json(body);
});

app.put('/user/:id', [checkToken, checkAdmin_Role], (req, res) => {
  let id = req.params.id;

  // let body = req.body;

  // parameters that can be updated
  let body = _.pick(req.body, ['name', 'email', 'img', 'role', 'status']);

  // parameter of findByIdAndUpdate of mongoose
  let options = {
    new: true,
    runValidators: true
  };
  User.findByIdAndUpdate(id, body, options, (err, userDB) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      user: userDB
    });
  });
});

app.delete('/user/:id', [checkToken, checkAdmin_Role], (req, res) => {
  let id = req.params.id;

  // Delete forever an item
  // User.findByIdAndRemove(id, (err, userDelete) => {
  //   if (err) {
  //     return res.status(400).json({
  //       ok: false,
  //       err
  //     });
  //   }

  //   if (!userDelete) {
  //     return res.status(400).json({
  //       ok: false,
  //       err: {
  //         message: 'User not found'
  //       }
  //     });

  //   }

  //   res.json({
  //     ok:true,
  //     user: userDelete
  //   })
  // })
  // res.json('delete Usuario');

  // Delete is change a status google

  let options = {
    new: true
  };

  let status = {
    status: false
  };
  User.findByIdAndUpdate(id, status, options, (err, userDB) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      user: userDB
    });
  });
});

module.exports = app;
