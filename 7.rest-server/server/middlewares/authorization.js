
const jwt = require('jsonwebtoken');

// =======================
// ======Check Token======
// =======================


let checkToken = (req, res, next) => {
  
  // take header witn name token
  let token = req.get('token');
  
  jwt.verify(token, process.env.SEED, (err, decoded) => {

    if (err) {
      return res.status(401).json({
        ok: false,
        err
      })
    }

    req.user = decoded.user;
    
    next();
    
  });

}

let checkAdmin_Role = (req, res, next) => {
  let user = req.user;
  let role = user.role;

  if (role === 'ADMIN_ROLE') {
    next()
  }else {
    return res.status(401).json({
      ok: false,
      message: 'user not ADMIN_ROLE'
    })
  }

}

module.exports = {
  checkToken,
  checkAdmin_Role
}