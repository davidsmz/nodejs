const argv = require('./config/yargs').argv;
const porHacer = require('./to-do/to-do')
const colors = require('colors');
// console.log(argv);

let comando = argv._[0]; // array de argumento de parametros de consola

switch (comando) {
  case 'crear': // argumento o parametro crear
    // console.log('crear por hacer');
    let tarea = porHacer.crear(argv.description);
    console.log(tarea);
    break;
  case 'listar':
    // console.log('listar tareas por hacer');
    let listado = porHacer.getListado();
    // console.log(listado);

    for(let tarea of listado) {
      console.log('====Por Hacer===='.green);
      console.log(tarea.description);
      console.log('Estado: ', tarea.completado);
      console.log('================='.green);
    }
    break;
  case 'actualizar':
    // console.log('actualizar tareas por hacer');
    let actualizado = porHacer.actualizar(argv.description, argv.completado);
    console.log(actualizado);
    break;
  case 'borrar':
    let borrado = porHacer.borrar(argv.description);
    console.log(borrado);
    break;
  default:
    console.log('comando no es reconocido');
    break;
}
