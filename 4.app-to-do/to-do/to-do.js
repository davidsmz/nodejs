const fs = require('fs');
const argv = require('../config/yargs').argv;


let listadoPorHacer = [];

const guardarDB = () => {
  let data = JSON.stringify(listadoPorHacer);
  fs.writeFile('db/data.json', data, (error) => {
    if (error) {
      throw new Error('No se pudo grabar los datos');
    }

    // console.log('Base de datos registrada!!');
  });
}

const cargarDB = () => {

  try {

    listadoPorHacer = require('../db/data.json');

  } catch (error) {
    listadoPorHacer = [];
  }
  // console.log(listadoPorHacer);
}

const crear = (description) => {

  cargarDB();

  let porHacer = {
    description,
    completado: false
  };

  listadoPorHacer.push(porHacer);
  guardarDB();

  return porHacer;

}

let getListado = () => {
  cargarDB();

  if (argv.completado === 'true') {
    let listadoTrue = listadoPorHacer.filter(tarea => {
      return tarea.completado === true;
    });
    // console.log('listado true');
    // console.log(listadoTrue);
    return listadoTrue;
  } 
  
  if(argv.completado === 'false') {
    let listadoFalse = listadoPorHacer.filter(tarea => {
      return tarea.completado === false;
    });
    // console.log('listado false');
    // console.log(listadoFalse);
    return listadoFalse;
  }
  // console.log(argv.completado === 'false');

  return listadoPorHacer;
}

let actualizar = (description, completado = true) => {
  cargarDB();

  let index = listadoPorHacer.findIndex(tarea => tarea.description === description);

  if(index >= 0) {
    listadoPorHacer[index].completado = completado;
    guardarDB();
    return true;
  } else {
    return false;
  }

}


const borrar = (description) => {
  cargarDB();

  let newListadoPorHacer = listadoPorHacer.filter(tarea => {
    return tarea.description !== description; 
  })

  if (listadoPorHacer.length === newListadoPorHacer.length) {
    return false;
  } else {
    listadoPorHacer = newListadoPorHacer;
    guardarDB();
    return true;
  }


}

module.exports = {
  crear,
  getListado,
  actualizar,
  borrar
}